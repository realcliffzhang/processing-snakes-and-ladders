# Processing - Snakes and Ladders
Positions of snakes and ladders are randomized for each round of gameplay.<br/>
Dimensions, screen size, snakes-to-blocks ratio and ladders-to-blocks ratio are adjustable.
![screenshot](https://i.imgur.com/D4duKr5.png)
![screenshot](https://i.imgur.com/CJjblim.png)
![screenshot](https://i.imgur.com/PzE4KUc.png)